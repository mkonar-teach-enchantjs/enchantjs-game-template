/** Create a game canvas **/
 
window.onload = myGame;  // Tell page to run myGame when page loads.
enchant();               // Do this to get some enchanted magic happening.
 
// Start of game code
function myGame() {
    // Make a new game with a 320x320 pixel canvas, 30 frames/sec.
    var game = new Core(320, 320);
    game.fps = 30;
 
    // Stuff you want to preload will go here.
 
    // Specify what should happen when the game loads.
    game.onload = function () {
        // -----------------------------------
        // ---- Game logic will go here. -----
        // -----------------------------------
    };
 
    // Start the game.
    game.start();
}
